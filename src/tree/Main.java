package tree;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){
        //String result = sExpression("(B,D) (D,E) (A,B) (C,F) (E,G) (A,C)"); // (A(B(D(E(G))))(C(F)))
        String result = sExpression("(A,B) (A,C) (B,D) (D,C)");

        System.out.println(result);
    }
    static class TreeNode {
        String name;
        TreeNode parent;
        TreeNode child1;
        TreeNode child2;

        String addChild(TreeNode child){
            if(child.parent != null)
                return "E3";

            if(this.child1 == null){
                this.child1 = child;
                child.parent = this;
                return "";
            }
            else if (this.child2 == null){
                this.child2 = child;
                child.parent = this;
                return "";
            }

            return "E1";
        }
        String print(){
            StringBuilder result = new StringBuilder();
            result.append("(" + this.name);
            if(child1 == null && child2 == null)
                result.append(")");
            else {
                if(child1 != null)
                    result.append(this.child1.print());
                if(child2 != null)
                    result.append(this.child2.print());

                result.append(")");
            }

            return result.toString();
        }
    }

    public static TreeNode findByName(List<TreeNode> listTreeNode, String name){
        TreeNode element = null;

        for (TreeNode treeNode: listTreeNode) {
            if(treeNode.name.equals(name))
                element = treeNode;
        }

        if(element == null){
            element = new TreeNode();
            element.name = name;
            listTreeNode.add(element);
        }

        return element;
    }

    public static List<TreeNode> findRoot(List<TreeNode> listTreeNode){
        List<TreeNode> listRoot = new ArrayList<>();

        for (TreeNode treeNode: listTreeNode) {
            if(treeNode.parent == null)
                listRoot.add(treeNode);
        }

        return listRoot;
    }

    public static String sExpression(String nodes){

        String restriction = "";
        List<TreeNode> listTreeNode = new ArrayList<>();

        String[] arr = nodes.split(" ");

        for (int i = 0; i<arr.length; i++){
            String parentName = arr[i].substring(1, 2);
            String childName = arr[i].substring(3, 4);

            TreeNode parentNode = findByName(listTreeNode, parentName);
            TreeNode childNode = findByName(listTreeNode, childName);

            restriction = parentNode.addChild(childNode);
            if(!restriction.isEmpty()){
                break;
            }

        }

        if(!restriction.isEmpty()){
            return restriction;
        }

        List<TreeNode> listRoot = findRoot(listTreeNode);

        if(listRoot.size()>1){
            return "E4";
        }

        return (listRoot.size()>0) ? listRoot.get(0).print() : "";
    }
}
