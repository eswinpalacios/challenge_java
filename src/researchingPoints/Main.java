package researchingPoints;

public class Main {

    public static void main(String[] args)
    {
        String result = canReach(1, 2, 5, 2); // YES
        System.out.println(result);
    }

    public static String canReach(int x1, int y1, int x2, int y2){
        StringBuilder result = new StringBuilder();

        if(x2<x1 || y2 < y1) return "No";
        if(x1 == x2 && y1 == y2) return "Yes";

        // type 1
        if((x1 + y1) <= x2){
            int newX = x1 + y1;
            result.append(canReach(newX, y1, x2, y2));
        }

        // type 2
        if((x1 + y1) <= y2) {
            int newY = y1 + x1;
            result.append(canReach(x1, newY, x2, y2));
        }

        return result.toString();
    }
}
