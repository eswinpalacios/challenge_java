package minimumMoves;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args){
        List<Integer> a1 = Arrays.asList(123, 220);
        List<Integer> a2 = Arrays.asList(320, 200);
        int result = miniumMoves(a1, a2);
        System.out.println(result); // 7
    }

    /*
    1. from location (x,y) to location (x+y, y)
    2. from location (x,y) to location (x, x+y).
     */

    public static int miniumMoves(List<Integer> arr1, List<Integer> arr2){

        int movesTotal = 0;

        for (int sizeArray = 0; sizeArray<arr1.size(); sizeArray++){
            String numbersArr1 = arr1.get(sizeArray).toString();
            String numbersArr2 = arr2.get(sizeArray).toString();

            for (int i=0; i<numbersArr1.length(); i++){
                int digit = Integer.parseInt(numbersArr1.substring(i, i+1));
                int digitMatch = Integer.parseInt(numbersArr2.substring(i, i+1));
                movesTotal += Math.abs(digit - digitMatch);
            }
        }

        return movesTotal;
    }
}
